package me.iloginovas.android.mychat.repository.message

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.room.InvalidationTracker
import me.iloginovas.android.mychat.data.AppDatabase
import me.iloginovas.android.mychat.data.MessageDao
import me.iloginovas.android.mychat.data.entity.MessageWithUsername
import timber.log.Timber
import kotlin.collections.ArrayList

class MessagePagingSourceFactory(private val database: AppDatabase) :
        () -> PagingSource<Int, MessageWithUsername> {

    private var pagingSource: MessagePagingSource? = null

    @Synchronized
    override fun invoke(): PagingSource<Int, MessageWithUsername> {
        pagingSource?.unsubscribeFromDatabaseChanges()

        val newPagingSource = MessagePagingSource(database)
        newPagingSource.subscribeOnDatabaseChanges()
        pagingSource = newPagingSource
        return newPagingSource
    }
}

class MessagePagingSource(
    private val database: AppDatabase
) : PagingSource<Int, MessageWithUsername>() {

    private val messageDao: MessageDao = database.messageDao()

    private val observer = object : InvalidationTracker.Observer("message") {
        override fun onInvalidated(tables: Set<String>) {
            invalidate()
        }
    }

    fun subscribeOnDatabaseChanges() {
        database.invalidationTracker.addObserver(observer)
    }

    fun unsubscribeFromDatabaseChanges() {
        database.invalidationTracker.removeObserver(observer)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MessageWithUsername> {
        return when (params) {
            is LoadParams.Refresh -> loadInitial(params)
            is LoadParams.Append -> loadOlderMessages(params)
            is LoadParams.Prepend -> loadNewerMessages(params)
        }
    }

    @ExperimentalPagingApi
    override fun getRefreshKey(state: PagingState<Int, MessageWithUsername>): Int? {
        val pos: Int = state.anchorPosition ?: return null
        state.closestItemToPosition(pos)?.let {
            return it.message.id
        }
        return null
    }

    /* Should be private but there is bug 'Impossible to set breakpoint in private suspend function during program execution'
     * https://youtrack.jetbrains.com/issue/KT-27810
     **/
    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun loadInitial(params: LoadParams.Refresh<Int>): LoadResult<Int, MessageWithUsername> {
        // Items sorted from new to old
        val resultList: List<MessageWithUsername> = when (val messageId = params.key) {
            null -> {
                messageDao.getLastMessages(params.loadSize)
                    .withoutGap(startMessageId = null, ascendingOrder = false)
            }
            else -> loadInitial(params, messageId)
        }

        val nextKey: Int? = resultList.lastOrNull()?.message?.id
        val prevKey: Int? =
            // params.key == null when it loads messages first time (no previous messages)
            if (params.key != null && resultList.size > 1)
                resultList.first().message.id
            else null

        Timber.d("loadInitial() loadSize=${params.loadSize}, initKey=${params.key}")
        Timber.d(
            """loadInitial() result count=${resultList.size}; 
            |first=${resultList.firstOrNull()?.message?.id} 
            |last=${resultList.lastOrNull()?.message?.id}""".trimMargin()
        )

        return LoadResult.Page(resultList, prevKey = prevKey, nextKey = nextKey)
    }

    /* Should be private but there is bug 'Impossible to set breakpoint in private suspend function during program execution'
     * https://youtrack.jetbrains.com/issue/KT-27810
     */
    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun loadOlderMessages(params: LoadParams.Append<Int>): LoadResult<Int, MessageWithUsername> {
        // Items sorted from new to old
        val resultList = messageDao.getMessagesOlderThan(params.key, params.loadSize)
            .withoutGap(params.key, ascendingOrder = false)

        val prevKey: Int? = resultList.firstOrNull()?.message?.id
        val nextKey: Int? =
            if (resultList.size > 1)
                resultList.last().message.id
            else null

        Timber.d("loadAfter() loadSize=${params.loadSize} key=${params.key}")
        Timber.d(
            """loadAfter() result count=${resultList.size}; 
            |first=${resultList.firstOrNull()?.message?.id} 
            |last=${resultList.lastOrNull()?.message?.id}""".trimMargin()
        )

        return LoadResult.Page(resultList, prevKey, nextKey)
    }

    /* Should be private but there is bug 'Impossible to set breakpoint in private suspend function during program execution'
     * https://youtrack.jetbrains.com/issue/KT-27810
     */
    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun loadNewerMessages(params: LoadParams.Prepend<Int>): LoadResult<Int, MessageWithUsername> {
        // Items sorted from new to old
        val resultList: List<MessageWithUsername> = messageDao
            .getMessagesNewerThan(params.key, params.loadSize)
            .withoutGap(params.key, ascendingOrder = true)
            .asReversed()

        val prevKey: Int? =
            if (resultList.size > 1)
                resultList.first().message.id
            else null

        val nextKey: Int? = resultList.lastOrNull()?.message?.id

        Timber.d("loadBefore() loadSize=${params.loadSize} key=${params.key}")
        Timber.d(
            """loadBefore() result count=${resultList.size}; 
            |first=${resultList.firstOrNull()?.message?.id} 
            |last=${resultList.lastOrNull()?.message?.id}""".trimMargin()
        )

        return LoadResult.Page(resultList, prevKey, nextKey)
    }

    private fun List<MessageWithUsername>.findGap(startMessageId: Int, ascendingOrder: Boolean): Int {
        val step: Int = if (ascendingOrder) 1 else -1
        var prevId: Int = startMessageId
        forEachIndexed { index, current ->
            if (current.message.id - prevId != step) return index
            else prevId = current.message.id
        }
        return -1
    }

    private fun List<MessageWithUsername>.withoutGap(
        startMessageId: Int?, ascendingOrder: Boolean
    ): List<MessageWithUsername> {

        if (size < 2) return this

        val step: Int = if (ascendingOrder) 1 else -1
        var prevId: Int = startMessageId ?: first().message.id
        val startIndex: Int = if (startMessageId != null) 0 else 1

        (startIndex..lastIndex).forEach { index ->
            val current = this[index]
            if (current.message.id - prevId != step) {
                return this.subList(0, index)
            }
            prevId = current.message.id
        }
        return this
    }

    /* Should be private but there is bug 'Impossible to set breakpoint in private suspend function during program execution'
     * https://youtrack.jetbrains.com/issue/KT-27810
     */
    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun loadInitial(params: LoadParams.Refresh<Int>, messageId: Int): List<MessageWithUsername> {
        // sorted in order from new to old
        val leftList: List<MessageWithUsername> = messageDao
            .getMessagesNewerThan(messageId, params.loadSize)
            .withoutGap(messageId, ascendingOrder = true)
            .asReversed()


        val keyElementList: List<MessageWithUsername> = messageDao
            .getMessageById(messageId).let {
                if (it == null) emptyList() else listOf(it)
            }

        // sorted in order from new to old
        val rightList: List<MessageWithUsername> = messageDao
            .getMessagesOlderThan(messageId, params.loadSize)
            .withoutGap(messageId, ascendingOrder = false)


        /*
         * The result list is composed of
         * [left_i_1, left_i_2, ..., left_i_l], [key], [right_j_1, right_j_2, ..., right_j_r]
         * where
         * [left_i] means an item with index i from leftList
         * [right_j] means an item with index j from rightList
         * [key] is item by the key (can be absent)
         *
         * Each list (left list or right list) can have size 0 or greater
         * but resulting list size should be not greater than [params.loadSize]
         */

        val leftListSize = leftList.size
        val rightListSize = rightList.size
        val preferredLeftListSize: Int = params.loadSize / 2 + keyElementList.size
        val preferredRightListSize: Int = params.loadSize / 2 - 1 + (params.loadSize % 2)

        return if (leftListSize >= preferredLeftListSize && rightListSize >= preferredRightListSize) {
            ArrayList<MessageWithUsername>(params.loadSize).apply {
                addAll(leftList.takeLast(preferredLeftListSize))
                addAll(keyElementList)
                addAll(rightList.take(preferredRightListSize))
            }

        } else if (leftListSize >= preferredLeftListSize) {
            val resultLeftListSize = leftListSize - rightListSize - keyElementList.size
            val totalSize = resultLeftListSize + rightListSize + keyElementList.size
            ArrayList<MessageWithUsername>(totalSize).apply {
                addAll(leftList.takeLast(resultLeftListSize))
                addAll(keyElementList)
                addAll(rightList)
            }

        } else if (rightListSize >= preferredRightListSize) {
            val resultRightListSize = rightListSize - leftListSize - keyElementList.size
            val totalSize = resultRightListSize + leftListSize + keyElementList.size
            ArrayList<MessageWithUsername>(totalSize).apply {
                addAll(leftList)
                addAll(keyElementList)
                addAll(rightList.take(resultRightListSize))
            }

        } else {
            val totalSize = leftListSize + keyElementList.size + rightListSize
            ArrayList<MessageWithUsername>(totalSize).apply {
                addAll(leftList)
                addAll(keyElementList)
                addAll(rightList)
            }
        }
    }
}
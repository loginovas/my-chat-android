package me.iloginovas.android.mychat.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(

    @PrimaryKey
    val id: Int,

    /** See spec in [me.iloginovas.android.mychat.validation.ClientSideUsernameValidator] */
    @ColumnInfo(collate = ColumnInfo.NOCASE)
    val name: String
)
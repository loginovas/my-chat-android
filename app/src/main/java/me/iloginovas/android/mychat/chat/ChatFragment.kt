package me.iloginovas.android.mychat.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_fragment.*
import kotlinx.android.synthetic.main.chat_fragment.view.*
import me.iloginovas.android.mychat.R
import me.iloginovas.android.mychat.repository.LoginResult
import me.iloginovas.android.mychat.model.Message

internal const val USER_ARG: String = "arg_user"

class ChatFragment : Fragment() {

    interface LogOutCallback {
        fun loggedOut()
    }

    companion object {
        fun newInstance(loginResult: LoginResult.Successful): ChatFragment {
            return ChatFragment().apply {
                arguments = Bundle().apply {
                    putBundle(USER_ARG, loginResult.toBundle())
                }
            }
        }
    }

    private lateinit var logOutCallback: LogOutCallback
    private val viewModel: ChatViewModel by viewModels()

    private lateinit var recyclerViewAdapter: PagingDataAdapter<Message, MessageViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logOutCallback = context as LogOutCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.chat_fragment, container, false)

        val recyclerView: RecyclerView = view.chat_msgList_recyclerView
        recyclerViewAdapter = MessageItemAdapter(requireContext().applicationContext)
        recyclerView.layoutManager = LinearLayoutManager(requireContext()).apply {
            reverseLayout = true
        }
        recyclerView.adapter = recyclerViewAdapter

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.isInitialized.observe(viewLifecycleOwner) { initialized: Boolean ->
            progressBar.visibility = if (initialized) View.GONE else View.VISIBLE
            chat_msgList_recyclerView.visibility = if (initialized) View.VISIBLE else View.INVISIBLE
        }

        viewModel.messageList.observe(viewLifecycleOwner) { messages: PagingData<Message> ->
            recyclerViewAdapter.submitData(viewLifecycleOwner.lifecycle, messages)
        }

        val messageEditText: EditText = chat_messageEditText
        chat_sendMessage_button.setOnClickListener {
            val text = messageEditText.text?.toString() ?: ""
            messageEditText.setText("")
            viewModel.sendMessage(text)
        }
    }
}
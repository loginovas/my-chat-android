package me.iloginovas.android.mychat.login

import android.app.Application
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import me.iloginovas.android.mychat.R
import me.iloginovas.android.mychat.repository.LoginRepository
import me.iloginovas.android.mychat.repository.LoginResult
import me.iloginovas.android.mychat.repository.UserCredentials
import me.iloginovas.android.mychat.validation.ClientSideUsernameValidator
import me.iloginovas.android.mychat.validation.ValidationResult
import timber.log.Timber
import java.util.concurrent.atomic.AtomicReference

enum class Mode { LOGIN, REGISTRATION }

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val context: Application
        inline get() = getApplication()

    private val loginRepository = AtomicReference<LoginRepository?>(null)

    val username: LiveData<String>
        get() = usernameLiveData

    val password: LiveData<String>
        get() = passwordLiveData

    val mode: LiveData<Mode>
        get() = modeLiveData

    val errorMessage: LiveData<ErrorMessage>
        get() = errorMessageLiveData

    val loggedInCallback: LiveData<LoginResult.Successful?>
        get() = loggedInCallbackLiveData

    val isLoggingInProgress: LiveData<Boolean>

    private val usernameLiveData = MutableLiveData<String>()
    private val passwordLiveData = MutableLiveData<String>()
    private val modeLiveData = MutableLiveData(Mode.LOGIN)
    private val errorMessageLiveData: MediatorLiveData<ErrorMessage>
    private val loginJobLiveData = MutableLiveData<Job?>(null)
    private val loggedInCallbackLiveData = MutableLiveData<LoginResult.Successful?>(null)

    init {
        errorMessageLiveData = createErrorMessageLiveData()
        isLoggingInProgress = Transformations.map(loginJobLiveData) { job -> job != null }
    }

    private fun createErrorMessageLiveData(): MediatorLiveData<ErrorMessage> =
        MediatorLiveData<ErrorMessage>().apply {
            addSource(username) {
                val newUsernameValue = it.trim()
                val errorMessage =
                    if (newUsernameValue.isBlank())
                        ErrorMessage.None
                    else when (val validationResult = ClientSideUsernameValidator.validate(context, newUsernameValue)) {
                        is ValidationResult.Success -> ErrorMessage.None
                        is ValidationResult.Failure -> ErrorMessage.Existing(validationResult.localizedMsg)
                    }

                value = errorMessage
            }
        }

    fun onUsernameChange(username: String) {
        if (username.trim() != usernameLiveData.value?.trim()) {
            usernameLiveData.value = username
        }
    }

    fun onPasswordChange(password: String) {
        if (password.trim() != passwordLiveData.value?.trim()) {
            passwordLiveData.value = password
        }
    }

    fun startLogin() {
        if (isLoggingInProgress.value == true) return

        loginJobLiveData.value = createLoginJob(mode.value!!)
        loginJobLiveData.value?.start()
    }

    fun cancelLogging() {
        loginJobLiveData.value?.cancel()
    }

    /**
     * Recent successfully logged username
     */
    fun getRecentUsername(): String {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getString(PREF_LOGIN_RECENT_USERNAME, "")!!
    }

    fun switchMode() {
        val newMode = when (mode.value!!) {
            Mode.LOGIN -> Mode.REGISTRATION
            Mode.REGISTRATION -> Mode.LOGIN
        }
        modeLiveData.value = newMode
    }

    private fun createLoginJob(mode: Mode): Job =
        viewModelScope.launch(start = CoroutineStart.LAZY) {
            try {
                val user = username.value?.trim().orEmpty()
                val password = password.value?.trim().orEmpty()
                val userCredentials = UserCredentials(user, password)

                val repository: LoginRepository = loginRepository.get() ?: kotlin.run {
                    LoginRepository.getInstance(context).also { loginRepository.set(it) }
                }

                val loginResult = when (mode) {
                    Mode.LOGIN -> repository.login(userCredentials)
                    Mode.REGISTRATION -> repository.register(userCredentials)
                }

                when (loginResult) {
                    is LoginResult.Successful -> internalLoginCallback.onSuccess(loginResult, user)
                    is LoginResult.Failed -> internalLoginCallback.onFailure(loginResult)
                }

            } catch (e: CancellationException) {
                Timber.d(e, "Logging was canceled")
                internalLoginCallback.onCancellation()

            } catch (e: Throwable) {
                Timber.e(e, "Exception occurred while logging")
                internalLoginCallback.onException(e)

            } finally {
                loginJobLiveData.value = null
            }
        }

    private val internalLoginCallback = object {

        fun onSuccess(result: LoginResult.Successful, username: String) {
            setRecentUsername(username)
            loggedInCallbackLiveData.value = result
        }

        fun onFailure(result: LoginResult.Failed) {
            errorMessageLiveData.value = ErrorMessage.Existing(result.localizedMsg)
        }

        fun onCancellation() {
            val errorMsg = context.getString(R.string.login_loggingWasCanceled)
            errorMessageLiveData.value = ErrorMessage.Existing(errorMsg)
        }

        fun onException(e: Throwable) {
            val errorMsg = context.getString(R.string.login_unknownLoggingError)
            errorMessageLiveData.value = ErrorMessage.Existing(errorMsg)
        }
    }

    private fun setRecentUsername(username: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PREF_LOGIN_RECENT_USERNAME, username)
            .apply()
    }

    sealed class ErrorMessage {
        object None : ErrorMessage()
        class Existing(val localizedErrorMsg: String) : ErrorMessage()
    }
}

private const val PREF_LOGIN_RECENT_USERNAME = "loginRecentUsername"
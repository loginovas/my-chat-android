package me.iloginovas.android.mychat.repository

import android.app.Application
import me.iloginovas.android.mychat.server.LoginService
import me.iloginovas.android.mychat.validation.ClientSideUsernameValidator
import me.iloginovas.android.mychat.validation.ValidationResult
import me.iloginovas.mychat.server.webapi.WebApi
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.IllegalStateException

class LoginRepositoryImpl(
    private val application: Application,
    private val loginService: LoginService,
    private val retrofit: Retrofit
) : LoginRepository {

    private val loginResponseConverter: Converter<ResponseBody, WebApi.LoginResponse>
            by lazy {
                retrofit.responseBodyConverter(WebApi.LoginResponse::class.java, arrayOf())
            }

    override suspend fun login(userCredentials: UserCredentials): LoginResult {
        val credentials = userCredentials.copy(username = userCredentials.username.trim())

        ClientSideUsernameValidator.validate(application, credentials.username).let {
            if (it is ValidationResult.Failure)
                return LoginResult.Failed(it.localizedMsg)
        }

        val response: Response<WebApi.LoginResponse> = loginService.login(credentials.fromModel())
        return response.toModel()
    }

    override suspend fun register(userCredentials: UserCredentials): LoginResult {
        val credentials = userCredentials.copy(username = userCredentials.username.trim())

        ClientSideUsernameValidator.validate(application, credentials.username).let {
            if (it is ValidationResult.Failure)
                return LoginResult.Failed(it.localizedMsg)
        }

        val response: Response<WebApi.LoginResponse> = loginService.register(credentials.fromModel())
        return response.toModel()
    }

    private fun Response<WebApi.LoginResponse>.toModel(): LoginResult {
        val loginResponse: WebApi.LoginResponse =
            if (isSuccessful)
                body()!!
            else {
                errorBody().let {
                    if (it == null) throw IllegalStateException("Error body is not set")
                    loginResponseConverter.convert(it)!!
                }
            }

        return loginResponse.toModel()
    }
}

private fun UserCredentials.fromModel() = WebApi.UserCredentials(username, password)

private fun WebApi.LoginResponse.toModel(): LoginResult {
    return if (successful) {
        if (user == null || sessionId == null)
            return LoginResult.Failed("Server error")

        LoginResult.Successful(user!!.name, user!!.id, sessionId!!)
    } else {
        LoginResult.Failed(errorMsg ?: "Server error")
    }
}
package me.iloginovas.android.mychat.server

import com.google.gson.Gson
import io.ktor.client.features.websocket.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.webapi.WebApi
import me.iloginovas.mychat.server.webapi.websocket.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.ConcurrentHashMap

interface RealtimeChatService {

    fun newMessageFlow(): SharedFlow<List<Message>>

    suspend fun sendMessage(text: String)

    suspend fun getLatestMessages(maxSize: Int): List<Message>

    suspend fun getMessagesNewerThan(messageId: Int, maxSize: Int): List<Message>

    suspend fun getMessagesOlderThan(messageId: Int, maxSize: Int): List<Message>

    suspend fun getUsers(userIds: List<Int>): List<WebApi.User>
}

class RealtimeChatServiceImpl(
    private val ws: DefaultClientWebSocketSession,
    private val gson: Gson
) : RealtimeChatService {

    private val newMessageFlow = MutableSharedFlow<List<Message>>(extraBufferCapacity = 24)

    private val responseRequestSupport = WsResponseRequestSupport(
        sendRequestFun = { request: WsRequest<*> ->
            ws.send(request.toJson())
        }
    )

    fun start(): Job = ws.launch {
        while (isActive) {
            val textFrame: Frame.Text =
                try {
                    ws.incoming.receive() as? Frame.Text ?: continue
                } catch (e: ClosedReceiveChannelException) {
                    ws.cancel()
                    Timber.d("Incoming channel was closed")
                    return@launch
                } catch (e: Throwable) {
                    Timber.d(e)
                    continue
                }

            onServerEvent(textFrame.readText())
        }
    }

    private suspend fun onServerEvent(json: String) {
        val eventContainer = gson.fromJson(json, EventContainer::class.java)
        when (val event: WebSocketEvent = eventContainer.event) {
            is WsServerEvents.NewMessages ->
                newMessageFlow.emit(event.messages)
            is WsResponse ->
                responseRequestSupport.onResponse(event)
        }
    }

    override fun newMessageFlow(): SharedFlow<List<Message>> =
        newMessageFlow

    override suspend fun sendMessage(text: String) {
        val message = WsClientEvents.SendMessage(text).toJson()
        ws.send(Frame.Text(message))
    }

    override suspend fun getLatestMessages(maxSize: Int): List<Message> {
        val request = WsRequests.GetMessages.Latest(preferredMaxCount = maxSize)
        return responseRequestSupport.request(request).messages
    }

    override suspend fun getMessagesNewerThan(messageId: Int, maxSize: Int): List<Message> {
        val request = WsRequests.GetMessages.NewerThan(messageId = messageId, preferredMaxCount = maxSize)
        return responseRequestSupport.request(request).messages
    }

    override suspend fun getMessagesOlderThan(messageId: Int, maxSize: Int): List<Message> {
        val request = WsRequests.GetMessages.OlderThan(messageId = messageId, preferredMaxCount = maxSize)
        return responseRequestSupport.request(request).messages
    }

    override suspend fun getUsers(userIds: List<Int>): List<WebApi.User> {
        val request = WsRequests.GetUsers(userIds = userIds)
        return responseRequestSupport.request(request).users
    }

    private fun WsClientEvent.toJson(): String {
        val eventContainer = EventContainer(this)
        return gson.toJson(eventContainer)
    }
}

class WsResponseRequestSupport(
    private val timeoutMillis: Long = 10000,
    private val sendRequestFun: suspend (WsRequest<*>) -> Unit
) {

    private val waitingRequests = ConcurrentHashMap<UUID, Channel<WsResponse>>()

    fun onResponse(response: WsResponse) {
        waitingRequests[response.id]?.offer(response)
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun <T : WsResponse> request(request: WsRequest<T>): T {
        val responseChannel = Channel<WsResponse>()
        waitingRequests[request.id] = responseChannel
        try {
            sendRequestFun(request)
            return withTimeout(timeMillis = timeoutMillis) {
                responseChannel.receive() as T
            }
        } finally {
            waitingRequests.remove(request.id)
        }

    }
}
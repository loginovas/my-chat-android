package me.iloginovas.android.mychat.repository

import android.app.Application
import me.iloginovas.android.mychat.server.RemoteServiceProvider

interface LoginRepository {

    companion object {
        suspend fun getInstance(application: Application): LoginRepository {
            val serviceProvider = RemoteServiceProvider.getInstance()
            return LoginRepositoryImpl(
                application,
                serviceProvider.loginService(),
                serviceProvider.retrofit()
            )
        }
    }

    suspend fun login(userCredentials: UserCredentials): LoginResult
    suspend fun register(userCredentials: UserCredentials): LoginResult

}

data class UserCredentials(val username: String, val password: String)

sealed class LoginResult {
    class Failed(val localizedMsg: String) : LoginResult()
    class Successful(
        val username: String,
        val userId: Int,
        val accessToken: String
    ) : LoginResult()
}


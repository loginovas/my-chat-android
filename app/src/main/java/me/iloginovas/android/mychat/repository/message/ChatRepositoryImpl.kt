package me.iloginovas.android.mychat.repository.message

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.paging.*
import androidx.room.withTransaction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import me.iloginovas.android.mychat.data.*
import me.iloginovas.android.mychat.data.entity.MessageEntity
import me.iloginovas.android.mychat.data.entity.MessageWithUsername
import me.iloginovas.android.mychat.data.entity.UserEntity
import me.iloginovas.android.mychat.repository.*
import me.iloginovas.android.mychat.model.Message
import me.iloginovas.android.mychat.model.SendingState
import me.iloginovas.android.mychat.model.User
import me.iloginovas.android.mychat.server.ChatServiceFactory
import me.iloginovas.android.mychat.server.RemoteServiceProvider
import timber.log.Timber
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import me.iloginovas.mychat.common.model.Message as MessageTransfer

/**
 * Use only after [init] call
 */
class ChatRepositoryImpl(
    val context: Context,
    val scope: CoroutineScope,
    val loginResult: LoginResult.Successful
) : ChatRepository {

    private lateinit var database: AppDatabase
    private lateinit var chatServiceFactory: ChatServiceFactory

    private val currentUser = User(loginResult.userId, loginResult.username)

    private val userCache = ConcurrentHashMap<Int, User>()

    suspend fun init() {
        database = AppDatabase.getInstance(context, currentUser.id)
        chatServiceFactory = RemoteServiceProvider.getInstance()
            .chatServiceFactory(scope, loginResult.accessToken)

//        database.userDao().getUser(currentUser.id) ?: run {
//            database.userDao().insertOrReplace(UserEntity(currentUser.id, currentUser.name))
//            database = AppDatabase.getInstance(context, currentUser.id)
//            chatServiceFactory = RemoteServiceProvider.getInstance()
//                .chatServiceFactory(scope, loginResult.accessToken)
//
//            (1..400).chunked(1000).forEach { chunk: List<Int> ->
//                val messages: List<MessageEntity> = chunk.map {
//                    MessageEntity(
//                        id = it,
//                        text = "$it",
//                        senderId = currentUser.id,
//                        sentAt = Date().time / 1000 + it
//                    )
//                }
//                database.messageDao().insertAll(messages)
//            }
//        }

//        scope.launch {
//            while (isActive) {
//                try {
//                    val service = chatServiceFactory.connect().first {
//                        it is ChatServiceFactory.State.ConnectionOpened
//                    }
//                    val chatService = (service as ChatServiceFactory.State.ConnectionOpened).chatService
//                    chatService.newMessageFlow().collect { newMessages: List<MessageTransfer> ->
//                        try {
//                            val users: List<Int> = newMessages.asSequence()
//                                .map { it.senderId }
//                                .distinct().filter { !userCache.containsKey(it) }
//                                .toList()
//                            val missingUsers = getMissingUsers(users)
//                            if (missingUsers.isNotEmpty()) {
//                                val newUsers = chatService.getUsers(missingUsers)
//                                val userEntities: List<UserEntity> = newUsers.map { UserEntity(it.id, it.name) }
//                                database.withTransaction {
//                                    database.userDao().insertAll(userEntities)
//                                }
//                            }
//                            database.withTransaction {
//                                val messageEntities = newMessages.map { MessageEntity(it.id, it.text, it.senderId, it.sentAt) }
//                                database.messageDao().insertAll(messageEntities)
//                            }
//                        } catch (e: Throwable) {
//                            Timber.d(e)
//                        }
//                    }
//                } catch (e: Throwable) {
//                    Timber.d(e)
//                }
//            }
//        }
    }

    @ExperimentalPagingApi
    override fun getMessagePagingData(): LiveData<PagingData<Message>> {
        val pager: Pager<Int, MessageWithUsername> = Pager(
            config = PagingConfig(
                pageSize = 25,
                enablePlaceholders = false
            ),
//            remoteMediator = MessageRemoteMediator(database, this, chatServiceFactory),
            pagingSourceFactory = MessagePagingSourceFactory(database)
        )

        return pager.liveData.map { pagingData: PagingData<MessageWithUsername> ->
            pagingData.map { it.toModel() }
        }
    }

    override suspend fun sendMessage(text: String) {
        chatServiceFactory.connect().value.let {
            if (it is ChatServiceFactory.State.ConnectionOpened) {
                it.chatService.sendMessage(text)
            }
        }
    }

    override suspend fun getMissingUsers(userIds: List<Int>): List<Int> {
        val existentUsers: List<UserEntity> =
            database.withTransaction {
                database.userDao().getUsers(userIds)
            }
        val existentIds = existentUsers.asSequence().map { it.id }.toHashSet()
        return userIds.filter { !existentIds.contains(it) }
    }

    private fun MessageWithUsername.toModel(): Message {
        val sentDate = Date(message.sentAt * 1000)
        return if (isMessageFromMe()) {
            Message.FromMe(message.id, message.text, SendingState.SentAt(sentDate))
        } else {
            val user: User = getUserOrCache(message.senderId, username)
            Message.FromAnother(message.id, message.text, user, sentDate)
        }
    }

    private fun MessageWithUsername.isMessageFromMe(): Boolean =
        message.senderId == currentUser.id

    private fun getUserOrCache(id: Int, name: String): User {
        userCache[id]?.let {
            return it
        }

        val user = User(id, name)
        userCache[user.id] = user
        return user
    }
}
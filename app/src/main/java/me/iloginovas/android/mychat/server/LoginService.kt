package me.iloginovas.android.mychat.server

import me.iloginovas.mychat.server.webapi.WebApi.LoginResponse
import me.iloginovas.mychat.server.webapi.WebApi.UserCredentials
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    @POST("user/login")
    suspend fun login(@Body credentials: UserCredentials): Response<LoginResponse>

    @POST("user/logout")
    suspend fun logout(@Header("SESSION") sessionId: String): Void

    @POST("user/registration")
    suspend fun register(@Body credentials: UserCredentials): Response<LoginResponse>
}
package me.iloginovas.android.mychat.data

import androidx.room.*
import me.iloginovas.android.mychat.data.entity.UserEntity

@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE id=:id")
    suspend fun getUser(id: Int): UserEntity?

    @Query("SELECT * FROM user WHERE id IN (:ids)")
    suspend fun getUsers(ids: List<Int>): List<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplace(user: UserEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<UserEntity>)

    @Update
    suspend fun update(user: UserEntity)

    @Delete
    suspend fun delete(user: UserEntity)
}
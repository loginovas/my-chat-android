package me.iloginovas.android.mychat.data.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded

data class MessageWithUsername(
    @Embedded
    val message: MessageEntity,

    @ColumnInfo(name = "username")
    val username: String
)
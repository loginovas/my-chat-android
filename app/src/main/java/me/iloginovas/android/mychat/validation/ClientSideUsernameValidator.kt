package me.iloginovas.android.mychat.validation

import android.app.Application
import me.iloginovas.android.mychat.R
import me.iloginovas.mychat.common.SuccessOrNot
import me.iloginovas.mychat.common.validation.UsernameErrorCause
import me.iloginovas.mychat.common.validation.UsernameValidator

object ClientSideUsernameValidator {

    fun validate(application: Application, username: String): ValidationResult {
        val errorCause: UsernameErrorCause =
            when (val validationResult = UsernameValidator.validate(username.trim())) {
                is SuccessOrNot.Success -> return ValidationResult.Success
                is SuccessOrNot.Failure -> validationResult.error
            }

        val getString: (resId: Int) -> String = application::getString

        val errorMsg: String =
            when (errorCause) {
                UsernameErrorCause.USERNAME_IS_EMPTY ->
                    getString(R.string.login_errorMsg_usernameIsEmpty)
                UsernameErrorCause.USERNAME_CONTAINS_INVALID_CHARACTERS ->
                    getString(R.string.login_errorMsg_usernameContainsInvalidCharacters)
                UsernameErrorCause.MAX_LENGTH_EXCEEDED ->
                    getString(R.string.login_errorMsg_maxLengthExceeded)
                UsernameErrorCause.FIRST_CHARACTER_IS_NUMBER ->
                    getString(R.string.login_errorMsg_firstCharacterShouldNotBeNumber)
                UsernameErrorCause.USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER ->
                    getString(R.string.login_errorMsg_atLeastOneLetterIsExpected)
            }

        return ValidationResult.Failure(errorMsg)
    }
}
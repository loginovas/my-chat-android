package me.iloginovas.android.mychat.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Group
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.login_form.*
import kotlinx.android.synthetic.main.login_form.view.*
import me.iloginovas.android.mychat.R
import me.iloginovas.android.mychat.repository.LoginResult
import me.iloginovas.android.mychat.login.LoginViewModel.ErrorMessage
import timber.log.Timber

class LoginFragment : Fragment() {

    interface LoginCallback {
        fun loggedIn(loginResult: LoginResult.Successful)
    }

    private lateinit var usernameTextInput: TextInputEditText

    private lateinit var userInputViewsGroup: Group
    private lateinit var inProgressViewsGroup: Group

    private val viewModel: LoginViewModel by viewModels()
    private var loginCallback: LoginCallback? = null

    companion object {
        fun newInstance(): LoginFragment = LoginFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginCallback = context as? LoginCallback
    }

    override fun onDetach() {
        super.onDetach()
        loginCallback = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.login_form, container, false)
        usernameTextInput = view.login_usernameTextInput

        userInputViewsGroup = view.login_userInputViewsGroup
        inProgressViewsGroup = view.login_inProgressViewsGroup
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ContentViewSizeOptimizationSupport.createAndStart(view)

        usernameTextInput.apply {
            doAfterTextChanged {
                viewModel.onUsernameChange(it?.toString().orEmpty())
            }

            if (savedInstanceState == null) {
                viewModel.onUsernameChange(viewModel.getRecentUsername())
            }
        }

        login_passwordTextInput.doAfterTextChanged {
            viewModel.onPasswordChange(it?.toString().orEmpty())
        }

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            onErrorMessageChanged(it)
        })

        viewModel.isLoggingInProgress.observe(viewLifecycleOwner, {
            onLoggingProgressStateChanged(it)
        })

        viewModel.mode.observe(viewLifecycleOwner, {
            onModeChanged(it)
        })

        loginButton.setOnClickListener { viewModel.startLogin() }
        login_cancelButton.setOnClickListener { viewModel.cancelLogging() }
        login_switchModeButton.setOnClickListener { viewModel.switchMode() }

        viewModel.loggedInCallback.observe(viewLifecycleOwner) { loginResult ->
            loginResult?.let {
                loginCallback?.loggedIn(it)
            }
        }
    }

    private fun onErrorMessageChanged(errorMessage: ErrorMessage) {
        fun setError(errorMsg: String?) {
            // It is workaround for https://issuetracker.google.com/issues/116747167
            if (login_usernameTextInputLayout.error != errorMsg) {
                login_usernameTextInputLayout.error = errorMsg
            }
        }

        when (errorMessage) {
            is ErrorMessage.None -> setError(null)
            is ErrorMessage.Existing -> setError(errorMessage.localizedErrorMsg)
        }
    }

    private fun onLoggingProgressStateChanged(isLoggingInProgress: Boolean) {
        userInputViewsGroup.visibility = if (isLoggingInProgress) View.GONE else View.VISIBLE
        inProgressViewsGroup.visibility = if (isLoggingInProgress) View.VISIBLE else View.GONE
    }

    private fun onModeChanged(mode: Mode) {
        when (mode) {
            Mode.REGISTRATION -> {
                loginButton.text = getString(R.string.login_registerButton_text)
                login_switchModeButton.text = getString(R.string.login_switchButton_goToLogin)
            }
            Mode.LOGIN -> {
                loginButton.text = getString(R.string.login_loginButton_text)
                login_switchModeButton.text = getString(R.string.login_switchButton_goToRegistration)
            }
        }
    }

    /**
     * Supports hiding the login header if there is not enough screen space
     * (especially when virtual keyboard is shown)
     * */
    private class ContentViewSizeOptimizationSupport private constructor(val parent: View) {

        private val minHeight: Int = calcMinHeight()

        companion object {
            fun createAndStart(parent: View): ContentViewSizeOptimizationSupport {
                return ContentViewSizeOptimizationSupport(parent).apply {
                    start()
                }
            }
        }

        fun start() {
            parent.addOnLayoutChangeListener { view,
                                               left, top, right, bottom,
                                               oldLeft, oldTop, oldRight, oldBottom ->
                val newHeight = bottom - top
                onWindowHeightChange(newHeight)
            }
        }

        private fun onWindowHeightChange(newHeight: Int) {
            Timber.d("onWindowHeightChange: newHeight=$newHeight")
            val isHeaderVisible = newHeight >= minHeight
            parent.login_header.apply {
                if (isVisible != isHeaderVisible) {
                    isVisible = isHeaderVisible
                    post {
                        requestLayout()
                    }
                }
            }
        }

        private fun calcMinHeight(): Int {
            val componentsToBeMeasured = parent.run {
                listOf(
                    login_header, login_usernameTextInputLayout, login_passwordTextInputLayout,
                    loginButton, login_switchModeButton
                )
            }

            var measuredHeight = 0
            componentsToBeMeasured.forEach {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                )
                measuredHeight += it.measuredHeight

                (it.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                    measuredHeight += bottomMargin + topMargin
                }
            }

            Timber.d("calcMinHeight: measuredHeight=$measuredHeight")
            return measuredHeight
        }
    }
}
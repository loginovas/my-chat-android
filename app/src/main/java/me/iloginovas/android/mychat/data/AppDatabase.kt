package me.iloginovas.android.mychat.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import me.iloginovas.android.mychat.data.entity.MessageEntity
import me.iloginovas.android.mychat.data.entity.UserEntity

@Database(entities = [UserEntity::class, MessageEntity::class], version = 1)
@TypeConverters(DbTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun messageDao(): MessageDao

    companion object {

        private var instance: AppDatabase? = null

        /**
         * Important to know that current user
         */
        @Synchronized
        fun getInstance(context: Context, userId: Int): AppDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext, AppDatabase::class.java, "chat-db-${userId}"
                ).build()
            }
            return instance!!
        }
    }
}
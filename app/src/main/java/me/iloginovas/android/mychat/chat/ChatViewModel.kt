package me.iloginovas.android.mychat.chat

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.iloginovas.android.mychat.repository.ChatRepository
import me.iloginovas.android.mychat.repository.LoginResult
import me.iloginovas.android.mychat.model.Message

sealed class MessageViewModel(val id: Int, val text: String) {

    class FromAnother(
        id: Int,
        text: String,
        val sendingTime: String,
        val from: String
    ) : MessageViewModel(id, text)

    class FromMe(
        id: Int,
        text: String,
        val sendingState: String,
    ) : MessageViewModel(id, text)
}

class ChatViewModel(application: Application, savedStateHandle: SavedStateHandle) : AndroidViewModel(application) {

    private val loginResult: LoginResult.Successful
    private val chatRepository = MutableLiveData<ChatRepository>()

    init {
        val bundle: Bundle = requireNotNull(savedStateHandle[USER_ARG])
        loginResult = bundle.toLoginResult()

        startChatRepositoryInitializationInBackground()
    }

    val isInitialized: LiveData<Boolean> = Transformations.map(chatRepository) {
        it != null
    }

    val messageList: LiveData<PagingData<Message>> = chatRepository
        .switchMap { chatRepository ->
            chatRepository.getMessagePagingData()
        }.cachedIn(viewModelScope)

    fun sendMessage(text: String) {
        if (text.isBlank()) return

        val repo = chatRepository.value ?: return
        viewModelScope.launch {
            repo.sendMessage(text)
        }
    }

    private fun startChatRepositoryInitializationInBackground() {
        viewModelScope.launch(Dispatchers.IO) {
            val repo = ChatRepository.create(getApplication(), viewModelScope, loginResult)
            chatRepository.postValue(repo)
        }
    }
}


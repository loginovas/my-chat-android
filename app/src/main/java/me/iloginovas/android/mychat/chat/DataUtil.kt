package me.iloginovas.android.mychat.chat

import android.os.Bundle
import me.iloginovas.android.mychat.repository.LoginResult

fun LoginResult.Successful.toBundle() = Bundle().also { bundle ->
    bundle.putString(LoginResult.Successful::username.name, username)
    bundle.putInt(LoginResult.Successful::userId.name, userId)
    bundle.putString(LoginResult.Successful::accessToken.name, accessToken)
}

fun Bundle.toLoginResult() = LoginResult.Successful(
    username = getString(LoginResult.Successful::username.name)!!,
    userId = getInt(LoginResult.Successful::userId.name),
    accessToken = getString(LoginResult.Successful::accessToken.name)!!
)
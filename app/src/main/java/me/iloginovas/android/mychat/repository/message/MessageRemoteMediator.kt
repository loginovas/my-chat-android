package me.iloginovas.android.mychat.repository.message

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import kotlinx.coroutines.flow.first
import me.iloginovas.android.mychat.data.AppDatabase
import me.iloginovas.android.mychat.data.entity.MessageEntity
import me.iloginovas.android.mychat.data.entity.MessageWithUsername
import me.iloginovas.android.mychat.data.entity.UserEntity
import me.iloginovas.android.mychat.repository.ChatRepository
import me.iloginovas.android.mychat.server.ChatServiceFactory
import me.iloginovas.android.mychat.server.RealtimeChatService
import me.iloginovas.mychat.server.webapi.WebApi
import timber.log.Timber
import me.iloginovas.mychat.common.model.Message as MessageTransfer

@Suppress("MemberVisibilityCanBePrivate")
@ExperimentalPagingApi
class MessageRemoteMediator(
    private val database: AppDatabase,
    private val chatRepository: ChatRepository,
    private val chatServiceFactory: ChatServiceFactory
) : RemoteMediator<Int, MessageWithUsername>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, MessageWithUsername>): MediatorResult {
        Timber.d("loadType=$loadType, state=$state")
        try {
            val newMessages: List<MessageTransfer> =
                when (loadType) {
                    LoadType.REFRESH -> loadRefresh(state)
                    LoadType.PREPEND -> loadPrepend(state)
                    LoadType.APPEND -> loadAppend(state)
                }
            Timber.d("loadType=$loadType, state=$state, result=(${newMessages.firstOrNull()}, ${newMessages.lastOrNull()})")

            if (newMessages.isEmpty()) return MediatorResult.Success(endOfPaginationReached = true)

            // We need to make sure that for each new message there is the User in the database
            val newUsers: List<WebApi.User> = kotlin.run {
                val missingUserIds: List<Int> = chatRepository.getMissingUsers(
                    newMessages.asSequence().map { it.senderId }.distinct().toList()
                )
                if (missingUserIds.isEmpty()) emptyList()
                else withRemoteService { getUsers(missingUserIds) }
            }
            Timber.d("Missing Users=$newUsers")

            database.withTransaction {
                if (newUsers.isNotEmpty()) {
                    val userEntities: List<UserEntity> = newUsers.map { UserEntity(it.id, it.name) }
                    database.userDao().insertAll(userEntities)
                }

                val messageEntities = newMessages.map { MessageEntity(it.id, it.text, it.senderId, it.sentAt) }
                database.messageDao().insertAll(messageEntities)
            }

            return MediatorResult.Success(endOfPaginationReached = false)

        } catch (e: Throwable) {
            Timber.d(e)
            return MediatorResult.Error(e)
        }
    }

    suspend fun loadRefresh(state: PagingState<Int, MessageWithUsername>): List<MessageTransfer> {
        val anchorMessageId: Int? = state.anchorPosition?.let { pos: Int ->
            state.closestItemToPosition(pos)?.message?.id
        }
        val pageSize = state.config.pageSize

        Timber.d("loadRefresh: anchorMessageId=$anchorMessageId")

        if (anchorMessageId == null)
            return withRemoteService { getLatestMessages(pageSize) }

        val startMessageId: Int = minOf(1, anchorMessageId - pageSize / 2)
        return withRemoteService { getMessagesNewerThan(startMessageId - 1, pageSize) }
    }

    suspend fun loadPrepend(state: PagingState<Int, MessageWithUsername>): List<MessageTransfer> {
        val latestMessage: MessageWithUsername = state.firstItemOrNull()!!
        return withRemoteService { getMessagesNewerThan(latestMessage.message.id, state.config.pageSize) }
    }

    suspend fun loadAppend(state: PagingState<Int, MessageWithUsername>): List<MessageTransfer> {
        val oldestMessage = state.lastItemOrNull()!!
        return withRemoteService { getMessagesOlderThan(oldestMessage.message.id, state.config.pageSize) }
    }

    private suspend inline fun <T> withRemoteService(
        onError: (Throwable) -> T = { throw it },
        onSuccess: RealtimeChatService.() -> T
    ): T {
        val state = chatServiceFactory.connect().first {
            it is ChatServiceFactory.State.ConnectionOpened
                    || it is ChatServiceFactory.State.ConnectionClosed
        }
        return when (state) {
            is ChatServiceFactory.State.ConnectionOpened ->
                onSuccess(state.chatService)
            else -> {
                val e: Throwable = (state as? ChatServiceFactory.State.ConnectionClosed)?.exception
                    ?: Exception("Connection error")
                onError(e)
            }
        }
    }
}
package me.iloginovas.android.mychat.chat

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import me.iloginovas.android.mychat.R
import me.iloginovas.android.mychat.model.Message
import me.iloginovas.android.mychat.model.SendingState
import me.iloginovas.android.mychat.databinding.ChatMessageFromAnotherBinding
import me.iloginovas.android.mychat.databinding.ChatMessageFromMeBinding
import java.util.*

sealed class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    class FromAnother(
        private val binding: ChatMessageFromAnotherBinding
    ) : MessageViewHolder(binding.root) {

        fun bind(message: MessageViewModel.FromAnother) {
            binding.apply {
                msg = message
                executePendingBindings()
            }
        }
    }

    class FromMe(
        private val binding: ChatMessageFromMeBinding
    ) : MessageViewHolder(binding.root) {

        fun bind(message: MessageViewModel.FromMe) {
            binding.apply {
                msg = message
                executePendingBindings()
            }
        }
    }
}

class MessageItemAdapter(private val appContext: Context) :
    PagingDataAdapter<Message, MessageViewHolder>(MessageItemDiffCallback) {

    enum class ItemType { NOT_LOADED, FROM_ME, FROM_ANOTHER }

    private val timeFormat = DateFormat.getTimeFormat(appContext)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ItemType.FROM_ME.ordinal -> {
                val binding = ChatMessageFromMeBinding.inflate(inflater, parent, false)
                MessageViewHolder.FromMe(binding)
            }
            ItemType.FROM_ANOTHER.ordinal -> {
                val binding = ChatMessageFromAnotherBinding.inflate(inflater, parent, false)
                MessageViewHolder.FromAnother(binding)
            }
            else -> throw IllegalStateException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        val itemType = when (getItem(position)) {
            null -> ItemType.NOT_LOADED
            is Message.FromMe -> ItemType.FROM_ME
            is Message.FromAnother -> ItemType.FROM_ANOTHER
        }
        return itemType.ordinal
    }

    override fun onBindViewHolder(viewHolder: MessageViewHolder, position: Int) {
        when (val message: Message? = getItem(position)) {
            is Message.FromAnother -> {
                require(viewHolder is MessageViewHolder.FromAnother)
                onBindViewHolder(viewHolder, message)
            }
            is Message.FromMe -> {
                require(viewHolder is MessageViewHolder.FromMe)
                onBindViewHolder(viewHolder, message)
            }
            else -> throw IllegalStateException()
        }
    }

    private fun onBindViewHolder(viewHolder: MessageViewHolder.FromAnother, message: Message.FromAnother) {
        val messageItem = MessageViewModel.FromAnother(
            id = message.id,
            text = message.text,
            sendingTime = message.sentAt.toTimeString(),
            from = message.from.name
        )
        viewHolder.bind(messageItem)
    }

    private fun onBindViewHolder(viewHolder: MessageViewHolder.FromMe, message: Message.FromMe) {
        val messageItem = MessageViewModel.FromMe(
            id = message.id,
            text = message.text,
            sendingState = when (val state: SendingState = message.sendingState) {
                is SendingState.SentAt -> state.dateTime.toTimeString()
                is SendingState.NotSent -> appContext.getString(R.string.chat_messageItem_sendingState_notSent)
            }
        )
        viewHolder.bind(messageItem)
    }

    private fun Date.toTimeString(): String = timeFormat.format(this)
}

private object MessageItemDiffCallback : DiffUtil.ItemCallback<Message>() {
    override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
        return areItemsTheSame(oldItem, newItem)
    }

}
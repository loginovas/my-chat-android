package me.iloginovas.android.mychat.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import kotlinx.coroutines.CoroutineScope
import me.iloginovas.android.mychat.repository.message.ChatRepositoryImpl
import me.iloginovas.android.mychat.model.Message

interface ChatRepository {

    companion object {

        /** May take a long time to complete */
        suspend fun create(
            context: Context,
            scope: CoroutineScope,
            loginResult: LoginResult.Successful
        ): ChatRepository {
            val chatRepository = ChatRepositoryImpl(context, scope, loginResult)
            chatRepository.init()
            return chatRepository
        }
    }

    fun getMessagePagingData(): LiveData<PagingData<Message>>

    suspend fun sendMessage(text: String)

    suspend fun getMissingUsers(userIds: List<Int>): List<Int>
}
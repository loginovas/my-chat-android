package me.iloginovas.android.mychat.model

import java.util.*

/**
 * Values of [id] grow in order from old to new which means that
 * Message[id=6] is newer than Message[id=6].
 *
 * In the current implementation, we require the [id] to be a sequence.
 * That is, the first value is 1 and each next value is greater by one (1, 2, 3,...).
 * For simplicity of implementation, we will not allow deleting messages for now.
 **/
sealed class Message {

    abstract val id: Int
    abstract val text: String

    class FromAnother(
        override val id: Int,
        override val text: String,
        val from: User,
        val sentAt: Date
    ) : Message()

    class FromMe(
        override val id: Int,
        override val text: String,
        val sendingState: SendingState
    ) : Message()
}

sealed class SendingState {
    class SentAt(val dateTime: Date) : SendingState()
    object NotSent : SendingState()
}
package me.iloginovas.android.mychat.data

import androidx.paging.PagingSource
import androidx.room.*
import me.iloginovas.android.mychat.data.entity.MessageEntity
import me.iloginovas.android.mychat.data.entity.MessageWithUsername
import java.util.*

@Dao
abstract class MessageDao {

    @Query(
        """SELECT message.*, user.name AS username FROM message 
        INNER JOIN user ON message.sender_id = user.id
        WHERE message.id =:id LIMIT 1"""
    )
    abstract suspend fun getMessageById(id: Int): MessageWithUsername?

    /**
     * @return sorted in order from new to old
     */
    @Query(
        """SELECT message.*, user.name AS username FROM message 
        INNER JOIN user ON message.sender_id = user.id 
        ORDER BY message.id DESC LIMIT :maxSize"""
    )
    abstract suspend fun getLastMessages(maxSize: Int): List<MessageWithUsername>

    @Query(
        """SELECT message.*, user.name AS username FROM message 
        INNER JOIN user ON message.sender_id = user.id
        WHERE message.id > :messageId
        ORDER BY message.id LIMIT :maxSize"""
    )
    abstract suspend fun getMessagesNewerThan(messageId: Int, maxSize: Int): List<MessageWithUsername>

    /**
     * @return sorted in order from new to old
     */
    @Query(
        """SELECT message.*, user.name AS username FROM message 
        INNER JOIN user ON message.sender_id = user.id
        WHERE message.id < :messageId
        ORDER BY  message.id DESC LIMIT :maxSize"""
    )
    abstract suspend fun getMessagesOlderThan(messageId: Int, maxSize: Int): List<MessageWithUsername>

    @Insert
    abstract suspend fun insert(message: MessageEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(messages: List<MessageEntity>)

    @Update
    abstract suspend fun update(message: MessageEntity)

    @Update
    abstract suspend fun update(messages: List<MessageEntity>)

    @Delete
    abstract suspend fun delete(message: MessageEntity)

    @Delete
    abstract suspend fun delete(messages: List<MessageEntity>)
}
package me.iloginovas.android.mychat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import me.iloginovas.android.mychat.chat.ChatFragment
import me.iloginovas.android.mychat.repository.LoginResult
import me.iloginovas.android.mychat.login.LoginFragment

class MainActivity : AppCompatActivity(), LoginFragment.LoginCallback, ChatFragment.LogOutCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val fragment = supportFragmentManager.findFragmentById(R.id.main_fragmentContainer)
        if (fragment == null) {
            setLoginFragment()
        }
    }

    private fun setLoginFragment() {
        supportFragmentManager.commit {
            val loginFragment = LoginFragment.newInstance()
            replace(R.id.main_fragmentContainer, loginFragment)
        }
    }

    override fun loggedIn(loginResult: LoginResult.Successful) {
        supportFragmentManager.commit {
            val chatFragment = ChatFragment.newInstance(loginResult)
            replace(R.id.main_fragmentContainer, chatFragment)
        }
    }

    override fun loggedOut() {
        setLoginFragment()
    }
}
package me.iloginovas.android.mychat.model

data class User(
    val id: Int,
    val name: String
)
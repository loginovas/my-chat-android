package me.iloginovas.android.mychat.server

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import me.iloginovas.android.mychat.server.ChatServiceFactory.State
import me.iloginovas.mychat.server.webapi.websocket.*
import okhttp3.*
import timber.log.Timber
import java.util.*

interface ChatServiceFactory {

    sealed class State {
        object TryingToConnect : State()
        class ConnectionClosed(val exception: Throwable? = null) : State()
        class ConnectionOpened(val chatService: RealtimeChatService) : State()
    }

    fun connect(): StateFlow<State>
    fun reconnect()
}

@Suppress("EXPERIMENTAL_API_USAGE")
class ChatServiceFactoryImpl(
    private val scope: CoroutineScope,
    private val host: String,
    private val port: Int,
    private val sessionId: String
) : ChatServiceFactory {

    private val stateFlow = MutableStateFlow<State>(State.TryingToConnect)
    private var connectionJob: Job? = null
    private val mutex = Any()

    init {
        scope.launch {
            stateFlow.subscriptionCount.first { it > 0 }
            startServiceIfNotRunning()
        }
    }

    private fun startServiceIfNotRunning() {
        synchronized(mutex) {
            val job = connectionJob
            if (job != null && job.isActive) return
            else connectionJob = getConnectionJob()
        }
    }

    override fun reconnect() {
        startServiceIfNotRunning()
    }

    override fun connect(): StateFlow<State> = stateFlow

    private fun getConnectionJob(): Job = scope.launch {
        connectFlow.collect { state: State ->
            stateFlow.value = state
        }
    }

    private val connectFlow = channelFlow {
        send(State.TryingToConnect)

        val ktorClient: HttpClient = RemoteServiceProvider.getInstance().ktorHttpClient()
        try {
            ktorClient.ws(host = host, port = port, path = "/ws?sessionId=$sessionId") {
                val service = RealtimeChatServiceImpl(
                    this,
                    RemoteServiceProvider.getInstance().gson()
                )
                val job = service.start()
                this@channelFlow.send(State.ConnectionOpened(service))
                job.join()
            }
        } catch (e: CancellationException) {
            Timber.d(e, "channelFlow canceled")
            //ignore
        } catch (e: Throwable) {
            send(State.ConnectionClosed(e))
            return@channelFlow
        }

        send(State.ConnectionClosed())
    }
}
package me.iloginovas.android.mychat.server

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.websocket.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import me.iloginovas.mychat.server.webapi.websocket.EventContainer
import me.iloginovas.mychat.server.webapi.websocket.EventContainerGsonDeserializer
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.atomic.AtomicReference
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.coroutineContext

interface RemoteServiceProvider {

    suspend fun loginService(): LoginService
    fun chatServiceFactory(scope: CoroutineScope, sessionId: String): ChatServiceFactory

    suspend fun okHttpClient(): OkHttpClient
    suspend fun ktorHttpClient(): HttpClient
    suspend fun retrofit(): Retrofit

    suspend fun gson(): Gson

    companion object {
        private var instance: RemoteServiceProvider? = null

        @Synchronized
        fun getInstance(): RemoteServiceProvider {
            if (instance == null) {
                instance = RemoteServiceProviderImpl()
            }
            return instance!!
        }
    }
}

class RemoteServiceProviderImpl : RemoteServiceProvider {
    private val mutex = Mutex()

    private val okHttpClientRef = AtomicReference<OkHttpClient>()
    private val ktorHttpClientRef = AtomicReference<HttpClient>()
    private val retrofitRef = AtomicReference<Retrofit>()
    private val gsonRef = AtomicReference<Gson>()

    override suspend fun okHttpClient(): OkHttpClient = okHttpClientRef.getOrCreate {
        OkHttpClient.Builder().build()
    }

    override suspend fun ktorHttpClient(): HttpClient = ktorHttpClientRef.getOrCreate {
        val okkHttpClient = okHttpClient()
        val okHttpEngine = OkHttp.create {
            preconfigured = okkHttpClient
        }
        HttpClient(okHttpEngine) { install(WebSockets) }
    }

    override suspend fun retrofit(): Retrofit = retrofitRef.getOrCreate {
        Timber.d("$host")
        Retrofit.Builder()
            .baseUrl("http://$host:$port/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient())
            .build()
    }

    override suspend fun loginService(): LoginService {
        return withWorkerDispatcher {
            retrofit().create(LoginService::class.java)
        }
    }

    override fun chatServiceFactory(scope: CoroutineScope, sessionId: String): ChatServiceFactory {
        return ChatServiceFactoryImpl(scope, host, port, sessionId)
    }

    override suspend fun gson(): Gson = gsonRef.getOrCreate {
        GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(EventContainer::class.java, EventContainerGsonDeserializer())
            .create()
    }

    private val host = "192.168.31.221"
//    private val host = "10.0.2.2"
    private val port = 8080


    //=============================
    // Utils
    //=============================
    private suspend fun <T> withWorkerDispatcher(
        context: CoroutineContext = EmptyCoroutineContext,
        block: suspend CoroutineScope.() -> T
    ): T = withContext(Dispatchers.Default + context, block)

    private suspend fun <T> AtomicReference<T>.getOrCreate(createFun: suspend () -> T): T {
        get()?.let { return it }

        if (coroutineContext[ReentrantMutexContextKey] != null)
            return createFun()

        mutex.withLock {
            get()?.let { return it }

            val instance = withWorkerDispatcher(ReentrantMutexContextElement()) {
                createFun()
            }
            set(instance)
            return instance
        }
    }

    private class ReentrantMutexContextElement(
        override val key: ReentrantMutexContextKey = ReentrantMutexContextKey
    ) : CoroutineContext.Element

    private object ReentrantMutexContextKey : CoroutineContext.Key<ReentrantMutexContextElement>
}
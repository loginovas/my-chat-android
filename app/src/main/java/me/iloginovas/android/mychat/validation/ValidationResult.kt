package me.iloginovas.android.mychat.validation

sealed class ValidationResult {
    object Success : ValidationResult()
    class Failure(val localizedMsg: String) : ValidationResult()
}
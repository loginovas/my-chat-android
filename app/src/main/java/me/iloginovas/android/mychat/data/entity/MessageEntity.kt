package me.iloginovas.android.mychat.data.entity

import androidx.room.*
import java.util.*

@Entity(
    tableName = "message",
    foreignKeys = [
        ForeignKey(entity = UserEntity::class, parentColumns = ["id"], childColumns = ["sender_id"])
    ],
    indices = [
        Index(value = ["sending_date"], unique = true),
        Index(value = ["sender_id"], unique = false)
    ]
)
data class MessageEntity(

    /**
     * Values grow in order from old to new which means that
     * Message[id=6] is newer than Message[id=5].
     *
     * In the current implementation, we require the [id] to be a sequence.
     * That is, the first value is 1 and each next value is greater by one (1, 2, 3,...).
     * For simplicity of implementation, we will not allow deleting messages for now.
     **/
    @PrimaryKey
    val id: Int,

    val text: String,

    /** User's ID */
    @ColumnInfo(name = "sender_id")
    val senderId: Int,

    /**
     * Stored as seconds since epoch.
     * @see DbTypeConverter.fromDate
     * @see DbTypeConverter.toDate
     */
    @ColumnInfo(name = "sending_date")
    val sentAt: Long
)
package me.iloginovas.android.mychat.data

import androidx.room.TypeConverter
import java.util.*

class DbTypeConverter {

    @TypeConverter
    fun fromDate(date: Date?): Long? = when (date) {
        null -> null
        else -> date.time / 1000
    }

    @TypeConverter
    fun toDate(secondsSinceEpoch: Long?): Date? =
        secondsSinceEpoch?.let { Date(it * 1000) }

    @TypeConverter
    fun fromUUID(uuid: UUID): String = uuid.toString()

    @TypeConverter
    fun toUUID(stringUUID: String): UUID = UUID.fromString(stringUUID)
}